<?php

namespace Drupal\ymb\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a ymb block.
 *
 * @Block(
 *   id = "ymb_ymb",
 *   admin_label = @Translation("Yandex Money Block"),
 *   category = @Translation("Yandex Money Blocks")
 * )
 */
class YmbBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'receiver' => '',
      'target' => '',
      'sum' => '200',
      'url' => '',
      'description' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['receiver'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Number of Yandex Money Wallet'),
      '#default_value' => $this->configuration['receiver'],
      '#maxlength' => 17,
      '#required' => TRUE,
    ];
    $form['target'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Payment purpose'),
      '#default_value' => $this->configuration['target'],
      '#required' => TRUE,
    ];
    $form['sum'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Amount to be prompted'),
      '#default_value' => $this->configuration['sum'],
      '#required' => TRUE,
      '#maxlength' => 4,
    ];
    $form['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link for redirecting'),
      '#default_value' => $this->configuration['url'],
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->configuration['description'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['receiver'] = $form_state->getValue('receiver');
    $this->configuration['target'] = $form_state->getValue('target');
    $this->configuration['sum'] = $form_state->getValue('sum');
    $this->configuration['url'] = $form_state->getValue('url');
    $this->configuration['description'] = $form_state->getValue('description');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [
      '#theme' => 'ymb_block',
      '#receiver' => $this->configuration['receiver'],
      '#target' => $this->configuration['target'],
      '#sum' => $this->configuration['sum'],
      '#url' => $this->configuration['url'],
      '#description' => $this->configuration['description'],
    ];
    return $build;
  }

}
